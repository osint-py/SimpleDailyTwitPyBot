*Simple example of 
a python bot that tweets daily.*

This bot is for demonstration and tutorial purposes.

Below a step by step introduction in creating a Twitter bot in python that runs from a gitlab server and therefor is not depending on you running your bot from your laptop every day.

Lets go!

<img alt="bring it on" src="https://media.giphy.com/media/U3PGvzpsVpAzGdhY6n/giphy.gif" width="300" />

# Bot raison d'exister
* Goal 1: Tweet a random joke
* Goal 2: Tweet daily a random joke
* Limititation: This bot will not interact with other users.
* Limititation: This bot will dowload any tweets from others.

**Table of Content**

[[_TOC_]]

# Prerequisites
1. You need an _email address/addresses_ for 
    1. Gitlab account.
    1. Giphy API key.
    1. Twitter developer account (leads to a Twitter API key).
    1. Twitter account for the bot.
1. You need a _phone number_ for:
    1. Twitter developer account (leads to a Twitter API key).
    1. Proton-email address - This is optional, you can also choose for another email address for backup.
1. Programming Environment 
    1. Make certain you have a [computer](pages/devenv.md) to write python code. This can be a _virtual_ or a _physical_ computer / laptop.
    1. This tutorial is tested on multiple flavours of Linux en ChromeOS.
    1. This tutorial will also work on MacOS.
    1. This tutorial is _not_ tested on Windows.
1. Skills
    1. You have completed a Python introduction course.
    1. You have completed a Git or Gitlab introduction course.

# Step 1 - Gitlab
## Create Gitlab Identities and Accounts
See page [Create Gitlab Identity](gitlab.md) on
how to create an account and what it all the other stuff as accounts and groups is.

## Create a gitlab project
There are first a few things to consider.

**TL;DR** : create a private repository and add a license file later.

1. __Private vs Public__ : Since we are using API Keys to twitter and perhaps other services, it is best to use a private repository. This makes it less difficult for other people to find your code and documentation, so that they can comment or change it. A public repository makes it easier for people to find your work and re-use it, but keeping your secrets is more difficult.
1. __README.md__: this is the main document in your repository, this is where your documentation starts. This file is written in clear text just as your code. This file is written in a language that is called Markdown. This is really easy to learn. See the page [Markdown and README.md](pages/markdown-readme.md) for some extra information.
1. a __License file__ is something you want to add. It is up to you if you want your code to be yours and only yours, or that you think everybody is free to copy and change it and apply it in a commercial product. There are many legal binding license files available. If you are new to this, I would advice to use the [License Selector](https://creativecommons.org/choose/) of the Creative Commons organisation. This will be enough for you. For the more experience people there, be inspired by this [License Selector](https://ufal.github.io/public-license-selector/) by [Institute of Formal and Applied Linguistics (ÚFAL)](https://github.com/ufal) and off course [Wikipedia](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licences). I prefer the most open Creative Commons license, but when in doubt the most strict is perhaps best. Do remember that if you use code from other people you should be respectful to their license choice. 


# Step 2 - Twitter

## Create Twitter Identities and Accounts
See the page [Twitter Accounts](pages/twitter.md)
for a description how to create accounts and store the API keys (etc).

_Remember_: you need to set read and write right to the application access tokens. 
When you missed this in the previous steps, the twitter documentation will help you.
See [Twitter - Developer Apps - App permissions](https://developer.twitter.com/en/docs/apps/app-permissions).



# Step 3 - Create Python code

* We are going to use [Tweepy](https://www.tweepy.org/) as "An easy-to-use Python library for accessing the Twitter API."
* We are going to tweet daily [Chuck Norris Jokes](pages/chucknorris.md) Jokes thanks to a great API.

## Create a python project directory

Best is to have all your coding projects in the same place on your computer.

Is this your first step then do the following.
_Note_: this manual does not include a Python starter course, So I hope you already have this.
```bash
$ cd
$ mkdir Projects
$ cd Projects
```

### Clone your respository from Gitlab to your machine
todo: Add some exra description
Best is to have for your coding projects 
each an individual Git project environment.

A GIT project is for me the same as a GIT repository.

I prefer Gitlab over Github, because gitlab is open source and their free plan delivers many many value.

```bash
# go to your home dir
$ cd

# go to your bot project dir
$ cd Projects

# clone your git-project (repository) 
# into the __tweepy-bots__ directory
$ git clone <url> tweepy-bots
```

## Create the python environment of your Bot

Now create the directory for our tweet bot, and add the Python modules we are going to us.

Python somethimes needs you to install modules, 
before you can use it in your script with ```import```.
The tool to install modules is called __pip__.

__Pip__ will also install depedencies a certain module requires to work.
To get a list of modules and their requirements and install this in one go, 
pip can create and use a file called __requirements.txt__.

Below a script to create a __requirements.txt__ file, 
which will be used to setup your bot-environment.


```bash
# go to your bot project dir
$ cd
$ cd Projects

#create your bot home dir.
$ mkdir tweepy-bots
# OR, when you have clones your project
$ cd tweepy-bots

# install the needed Python modules
$ cd tweepy-bots
$ python3 -m venv venv
$ source ./venv/bin/activate
$ sudo pip3 install requests
$ sudo pip3 install tweepy

# create an overview of the needed 
# Python modules in requirements.txt
$ pip3 freeze > requirements.txt
```
-- [RealPython - How to Make a Twitter Bot in Python With Tweepy by Miguel Garcia][tweepy-bot-guide]

### Minimize your Bot evironment
There is a way to reduce and even minimize the content of your requirements.txt. This is a bit more advanced step.

The trick is to install __pip-tools__ and use
the tool __pip-compile__ 
to determine the environemnt 
based on the modules defined 
in a file e.g. __recuirements.in__.

The modules you specificy 
in the __requirements.in__ file 
are the same as specified in your __.py__ file 
with the __import__ calls.

See in this repository the __requirements.in__
file as example.

```bash
# go to your home dir
$ cd

# go to your bot project dir
$ cd Projects
$ cd tweepy-bots

# install pip-tools which includes pip-compile
$ sudo pip3 install pip-tools

# Insert per line the module that is imported in the script / or needs to be installed via pip
$ vi requirements.in

# create your minimal requirements.txt
$ pip-compile requirements.in > requirements.txt
```
-- [Stackoverflow - Reducing requirements.txt](https://stackoverflow.com/questions/63655310/reducing-requirements-txt) on how to install and use [pip-tools](https://github.com/jazzband/pip-tools).

## Create a secret key file
In the twitter step you have collected 5 codes of which we are going to use 4.

First make certain you are in your project directory.
```bash
$ cd
$ cd Projects
$ cd tweepy-bots
```

Second make certain there is no keys file. 
this code renames the keys file if there is any.
```bash
$ mv keys keys.old
```

Replace in the code below the text between the "...." with your codes.
```bash
$ touch keys
$ echo "CONSUMER_KEY" >> keys
$ echo "CONSUMER_SECRET" >> keys
$ echo "ACCESS_TOKEN" >> keys
$ echo "ACCESS_TOKEN_SECRET" >> keys
```

## Test tweepy with twitter credentials

_file_: _tweepy-auth.py_
```python
import tweepy

#open a file called "keys" (no extention)
# with keys and tokens for Twitter
keyFile = open('keys', 'r')
lines = keyFile.readlines()
CONSUMER_KEY        = lines[0].rstrip()
CONSUMER_SECRET     = lines[1].rstrip()
ACCESS_TOKEN        = lines[2].rstrip()
ACCESS_TOKEN_SECRET = lines[3].rstrip()
keyFile.close()

# Authenticate to Twitter
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

# Create tweepy interface to twitter
api = tweepy.API(auth)

# Verify via the tweepy interface if twitter authenticates us
try:
    api.verify_credentials()
    print("Authentication OK")
except:
    print("Error during authentication")

```

-- [Stackoverflow - How can I read the API authentication data from a file using Python? ](https://stackoverflow.com/questions/24392515/how-can-i-read-the-api-authentication-data-from-a-file-using-python/24392516),
[Digital Ocean - How To Authenticate a Python Application with Twitter using Tweepy on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-authenticate-a-python-application-with-twitter-using-tweepy-on-ubuntu-14-04)

## Test tweepy with twitter post

_file_: _tweepy-tweet.py_
```python
import tweepy

#open a file called "keys" (no extention)
# with keys and tokens for Twitter
keyFile = open('keys', 'r')
lines = keyFile.readlines()
CONSUMER_KEY        = lines[0].rstrip()
CONSUMER_SECRET     = lines[1].rstrip()
ACCESS_TOKEN        = lines[2].rstrip()
ACCESS_TOKEN_SECRET = lines[3].rstrip()
keyFile.close()

# Authenticate to Twitter
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

# Create tweepy interface to twitter
api = tweepy.API(auth)

# post tweetText to twitter via the tweepy interface
tweetText = "Made with Tweepy"
tweet = api.update_status(tweetText)

```

## Tweet Chuck Norris Joke
We need to send a request for a Joke to the ChuckNorris.io service.
So we need to add the module _requests_ and issue the request.
    
_file_: _tweepy-tweet-joke.py_
```python
import tweepy
import requests

#open a file called "keys" (no extention)
# with keys and tokens for Twitter
keyFile = open('keys', 'r')
lines = keyFile.readlines()
CONSUMER_KEY        = lines[0].rstrip()
CONSUMER_SECRET     = lines[1].rstrip()
ACCESS_TOKEN        = lines[2].rstrip()
ACCESS_TOKEN_SECRET = lines[3].rstrip()
keyFile.close()
url_joke = 'https://api.chucknorris.io/jokes/random'

# Authenticate to Twitter
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

# Create tweepy interface to twitter
api = tweepy.API(auth)

# Request a random joke
resp=requests.get(url_joke)
data = resp.json()

# post tweetText to twitter via the tweepy interface
tweetText = data["value"]
tweet = api.update_status(tweetText)
```

-- [Stackoverflow - Looking for advice in Python on how to get my while loop to break](https://stackoverflow.com/questions/63183041/looking-for-advice-in-python-on-how-to-get-my-while-loop-to-break),
[W3Schools - Python request module](https://www.w3schools.com/python/module_requests.asp),
[Requests Documentation](https://requests.readthedocs.io/en/master/user/quickstart/)

# Step 4 - Run Script via Gitlab CI

## Provide gitlab job your twitter secrets

Since you do now want your secrets publicly in the gitlab repository,
you do not want to put your _keys_ file in the repository.
This was prevented by adding the _keys_ file in the _.gitignore_ file.

But how to get the secrets into the Gitlab server when it runs our joke script?

You can create a secret file in Gitlab via the browser.
On their page [Create a custom variable in the UI][gitlab-evn-var-ci] you can read the step-by-step procedure.

1. Go to your project’s **Settings > CI/CD** and expand the **Variables** section.
1. Click the **Add Variable** button. In the **Add variable** modal, fill in the details:
    1. Key: _KEYS_
    1. Value: _content of your keys file, created earlier_
    1. Type: _file_
    1. Environment scope: _All_
    1. Protected: _Yes_
    1. Mask variable: _No_


### Public repository safety
Your twitter secrets will be saved in Gitlab environment variables.
These environment variables are exposed via:
1. the admin pannel (previous step) 
1. via your code being run in the pipeline.
    1. via the _.gitlab-ci.yml_ file, 
        1. by calling the environment value directly or 
        1. by let it run code different then the _tweepy_tweet_joke.py_ file. 
    1. via the log_files of the pipelines in the "Jobs" tab of the "CI / CD" menu.

**Via the admin panel** : 
By default this is pretty good protected by gitlab.

"To protect, update, or unprotected an environment, you need to have at least Maintainer permissions." 
- [Gitlab - Protected environment](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

"Variables can only be updated or viewed by project members with maintainer permissions." 
- [Gitlab - Create a custom variable in the UI](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui)

This is why it is important to set 1) your environments variable to **Protected** in the previous step,
and 2) don't give everybody the role of **Maintainer**.

**Via the log files** : 
By default, in a public project, 
the CI/CD Pipelines, including the jobs, 
are only visible for people that haves given access to the project.
Best is to change this to _Only Project Members_. 
Project Members include _Maintainers_ as well as _Developers_.


**Via the .gitlab-ci.yml and via another .py file**:
This can be mitigates by making sure that if someone offers an improvement of the code,
via a _[Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html)_ this is validated by someone you trust (e.g. yourself).
This is done via [Merge Request Approval](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html)

It is important that you make certain the Merge Approval rules can not be disabled via a merge request,
this is enabled by default
([Gitlab Documentation](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#merge-request-approvals-project-settings)).

"By default, projects are configured to prevent merge requests from being approved by their own authors" - [Gitlab Documentation](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#merge-request-approvals-project-settings)

"To protect a branch, you need to have at least Maintainer permission level. Note that the master branch is protected by default."
([Gitlab Documentation - Protected Branches](https://docs.gitlab.com/ee/user/project/protected_branches.html)). 
A protected branch needs to have maintainer approval for merge requests 
([Gitlab Documentation - Protected branches flow](https://docs.gitlab.com/ee/user/project/merge_requests/authorization_for_merge_requests.html#protected-branch-flow)). This is a good thing! 

For more information on Permissions, see the [Gitlab Documentation on Permissions](https://docs.gitlab.com/13.6/ee/user/permissions.html).

**Concluding**: 
1. The _.gitlab-ci.yml_ file makes certain that _tweepy-tweet-joke.py_ can only be run by the pipeline.
1. The _.gitlab-ci.yml_ file makes certain that the pipeline will only run by the scheduler. 
1. The scheduler can only be forced to run by project members.
1. The environment variables of the pipeline can only be changed by the project members. 
1. The environment variables of the pipeline can only be read direct and indirect by the _.gitlab-ci.yml_ file.
1. The _.gitlab-ci.yml_ file can only be changed by project members.
1. The _tweepy-tweet-joke.py_ can only be changes (via approval) by project members.
1. Your secrets are save, for random people, when you use a public repository.
1. Your secrets are not save for code added to your project via Merge requests, thus be carefull with what your approve.
1. Your secrets are not save for your project members, thus be careful with who you add to your project.

### Other safety precautions 

Gitlab also writes on extra security [documentation][gitlab-env-var].
This is part of the full version of the _.gitlab-ci.yml_ further on in this README.md

[gitlab-env-var]: https://docs.gitlab.com/ee/ci/variables/README.html#protected-secret-variables
[gitlab-evn-var-ci]: https://docs.gitlab.com/ee/ci/variables/README.html#create-a-custom-variable-in-the-ui


## Define the automated job - Pipeline

In gitlab automated jobs are called **Pipelines**.
Pipelines are defines in a file called _.gitlab-ci.yml_ 
in a format called [YAML](https://en.wikipedia.org/wiki/YAML) (YAML Ain't Markup Language")
 
Below is a minimal example, which tweets a joke to our twitter account
via the same command as on our own computer: `python3 code-examples/tweepy-tweet-joke.py`

This pipeline consist of one part.
A part of a pipeline is called a stage.

The command `stages: - tweet` defines our one stage called _tweet_.

The command `pip3 install -r requirements.txt` installs the needed python modules like `requests`.

The command `cat $KEYS > keys` moves the keys from gitlab into a file, just like on our computer.

The _tweet_ stage is configures to run **only and only** 
when it is called by the scheduler.
This is done via `  only: - schedules`

### Minimal working Example pipeline

Save 
_file_: _.gitlab-ci-simple.yml_
in your repositopy as
_file_: _.gitlab-ci.yml_


```yaml
image: python:latest

stages:
  - tweet

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

before_script:
  - pip3 install -r requirements.txt
  - cat $KEYS > keys

tweet:
  stage: tweet
  script:
  - python3 code-examples/tweepy-tweet-joke.py
  only: 
    - schedules
```

## Schedule the automated job

This is done via the gitlab website.
On the right side menu there is the option *CI/CD* with the sub-option *Scheduler* below it.


## Advanced Pipeline

### Pipeline including...

**Static Code testing on secrets**: 
This example checks the code after each commit two things 1) if per accident secrets are in the code 2) if the license file is available.

"The Secret Detection analyzer includes Gitleaks and TruffleHog checks." - [Gitlab Documentation - Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)

**Static Code testing on License**: 
It is always a good thing to include a license file. This if for over 30 years the standard for a reason ;)

The Gitlab License scanner does not only scan your own files but also checks dependencies. This is no guarantee but every little help helpt. [Gitlab Documentation - License Compliance](https://docs.gitlab.com/ee/user/compliance/license_compliance/)

**Be Safe! Stay up to date!**: 
The "tweet" stage _upgrades_ and installs python3, loads the twitter secrets and 
runs the script that tweets the joke.

For who is experienced with python most upgrade, install, pip comamnds will be familiar.

_Note_: Why explicit Python3? To help the world move away from Python2 :)



_file_: _.gitlab-ci-full.yml_

```yaml
image: python:latest

stages:
  - analyse:code
  - tweet

  # https://docs.gitlab.com/ee/user/application_security/secret_detection/
include:
  - template: Secret-Detection.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml

license_scanning:
  stage: analyse:code
  needs: []
  
.secret-analyzer:
  stage: analyse:code
  needs: []

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# before_script:
  # action in tweet:script are best in before_script.
  # but secret detection does not allow this

tweet:
  stage: tweet
  script:
  - apt-get update -qy # Make certain all is up to date and save.
  - apt-get upgrade -qy
  - apt-get install -y python3-dev python3-pip
  - python3 -V         # Print out python version for debugging
  - pip3 install -r requirements.txt
  - cat $KEYS > keys
  - python3 code-examples/tweepy-tweet-joke.py
  only: 
    - schedules
```

# Homework
1. Try to add a gliphy image to the tweet
1. Make certain, before tweeting, that the tweet does not exceed the tweet-character-limit.

[botwiki]: https://botwiki.org/
[botwiki-twitter]:https://botwiki.org/resource/tutorial/how-to-create-a-twitter-app/
[botmakers]: https://botmakers.org/
[bot-guide]: https://botwiki.org/resource/tutorial/how-to-make-a-twitter-bot-the-definitive-guide/
[botwiki-python]: https://botwiki.org/resources/twitterbots/#tutorials-python

[tweepy-bot-guide]: https://realpython.com/twitter-bot-python-tweepy/
[tweepy-bot-doc]:http://docs.tweepy.org/en/latest/getting_started.html#hello-tweepy

[gitlab-cron-ds]: https://towardsdatascience.com/using-gitlabs-ci-for-periodic-data-mining-b3cc314ecd85
[gitlab-cron-tut]: https://www.freecodecamp.org/news/56-seconds-to-get-gitlab-to-do-periodic-jobs-for-you-6a731b977559/
[gitlab-ci-cron]: https://docs.gitlab.com/ee/ci/pipelines/schedules.html

[open-api-lib]: https://www.programmableweb.com/category/history/api
[botwiki-api-lib]: https://botwiki.org/resources/



[chuck-norris-api-doc]: https://rapidapi.com/matchilling/api/chuck-norris
[chuck-norris]: https://api.chucknorris.io/

[giphy-dev-doc]: https://developers.giphy.com/docs/resource/
[giphy-dev-doc-random]: https://developers.giphy.com/docs/api/endpoint#random
[giphy-request-key]: https://support.giphy.com/hc/en-us/articles/360020283431-Request-A-GIPHY-API-Key

[gitlab-md-slides]: https://about.gitlab.com/handbook/markdown-guide/#embed-documents

# License
Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)

```
#   You are free to:
#   Share — copy and redistribute the material in any medium or format.
#   Adapt — remix, transform, and build upon the material for any purpose, even commercially.
#   The licensor cannot revoke these freedoms as long as you follow the license terms.
#    
#    You can copy, modify, distribute and perform the work,  
#    even for commercial purposes, all without asking permission.
#    
#    Under the following terms:
#    Attribution — You must give appropriate credit, provide a link to the license, 
#                  and indicate if changes were made. You may do so in any reasonable manner, 
#                  but not in any way that suggests the licensor endorses you or your use.
#    ShareAlike — If you remix, transform, or build upon the material, 
#                 you must distribute your contributions under the same license as the original.
#    No additional restrictions — You may not apply legal terms or technological measures that 
#                 legally restrict others from doing anything the license permits.
#    
#    For more see the file 'LICENSE' for copying permission.
```
