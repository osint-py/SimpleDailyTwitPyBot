
## Inspiration on Readme.md
* Github user PurpleBooth wrote: "[A template to make good README.md][readme01]"
* Github user jxson wrote: "[README.md template][readme02]"
* Medium user meakaakka wrote: "[A Beginners Guide to writing a Kickass README][readme03]"

## Inspiration on Markdown Syntax
* GitHub   
  * Github user __adam-p__ wrote: "[Markdown Cheatsheet][markdown01]"
  * __Github Guides__ wrote: "[Markdown Syntax (pdf)][markdown02]"
  * __Github Guides__ wrote: "[Mastering Markdown (Wiki)][markdown03]"
  * Github user __tchapi__ wrote: "[Markdown Cheatsheet for Github][markdown04]"
* GitLab
  * __GitLab documentation__ - Markdown wrote: "[GitLab Flavored Markdown (GFM)][markdown05]"
  * __GitLab Team Handbook__ - Markdown Guide wrote: "[Markdown kramdown Style Guide for about.GitLab.com][markdown06]"
  * __Gitlab FOSS__ write: "[GitLab Markdown - This Markdown guide is valid only for GitLab's internal Markdown rendering system for entries and files.][markdown07]"
* Other
    * The original [Markdown Syntax Guide](https://daringfireball.net/projects/markdown/syntax) at Daring Fireball is an excellent resource for a detailed explanation of standard Markdown. 

[readme01]: https://gist.github.com/PurpleBooth/109311bb0361f32d87a2
[readme02]: https://gist.github.com/jxson/1784669
[readme03]: https://medium.com/@meakaakka/a-beginners-guide-to-writing-a-kickass-readme-7ac01da88ab3

[markdown01]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
[markdown02]: https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf
[markdown03]: https://guides.github.com/features/mastering-markdown/ 
[markdown04]: https://github.com/tchapi/markdown-cheatsheet

[markdown05]: https://docs.gitlab.com/ee/user/markdown.html
[markdown06]: https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/
[markdown07]: https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/user/markdown.md