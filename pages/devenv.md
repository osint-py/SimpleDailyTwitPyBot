## Development - Programming Environment 
To write documentation and to create the scheduler to daily run your bot
you can use gitlab and the integrated editor (webIDE).

To write code you can use the webIDE in gitlab, but
you can not use gitlab to run and test your code.

To run and test your code you need a physical or virtual computer.
This computer needs to have a code editor to _write_ in, python to _translate_ the code to computer laguange,
and a computer to _run_ the computer language on.

For a virtual computer you can use Cloud9. This includes the editor, python and can run the code.

For a physical computer you can use almost everything from cheap old computers, cheap tiny computers to expensive ones.

My personal favorites are Cloud9 in the cloud or Sublime/Vim on my laptop.

## Python Tutorials

See coursera, codeacademy etc for help in this.
