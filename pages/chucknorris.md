## Get a random joke
```
GET https://api.chucknorris.io/jokes/random
```

returns a JSON:

```json
{
"icon_url" : "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
"id" : "tlRJKIjKR9G1di_z-kpfUg",
"url" : "",
"value" : "Do you know why Chuck Norris doesn't have hair on his balls.....?????......????? because hair doesn't grow on steel!"
}
```
