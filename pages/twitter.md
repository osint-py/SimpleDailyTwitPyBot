How to retrieve accounts and credentials to access twitter services and tweet with your bot.

[[_TOC_]]

# Create Twitter Accounts

Basically you need the following
1. a Developer Account, to connect your identity with your bot (code) and twitter accounts where you want to tweet on.
1. Your own Twitter Account (connected to the Developer Account)
1. A Twitter account where you want your bod to tweet.

_Note_ : You can choose to let your code tweet to the same twitter account you have linked to your developer account. This saves you creating a second identity but also has this as downside. 


![Twitter Authorisation and Identification](https://i.imgur.com/uVMu2sB.png)

[Source: Google Slides deck](https://docs.google.com/presentation/d/1pIMNDOnBQXu5-P86OZARXRZCfvU_mCdFs4R5cgQALlc/edit?usp=sharing).


There are various blogs providing a similar howto on creating twitter accounts.
as for example: 
1. [How to create a Twitter bot - by Molly White on Wednesday, March 18, 2015 ](https://blog.mollywhite.net/how-to-create-a-twitter-bot/)
1. [How To Authenticate a Python Application with Twitter using Tweepy on Ubuntu 14.04 Ubuntu By erm on February 4, 2015](https://www.digitalocean.com/community/tutorials/how-to-authenticate-a-python-application-with-twitter-using-tweepy-on-ubuntu-14-04)

## Prerequisites
1. email address (for twitter account and developer account)
1. phone number (for developer account)

## Decide your account and identity structure

What do you want?

### Develop for an organisation
this tutorial is to simple for you, go to coursera, codeacademy etc and follow a real course

### Develop a bot separate to your own account
Most obvious is to connect your developer account to your personal account,
and develop a bot (code) and tweet to a separate dedicated twitter account.
This way twitter and everybody can see that this is your bot (openness),
and let your learn the most.

### Develop a bit as side-quest just for fun.
create a separate bot persona, and let your developer-persona be the same as 
your bot persona.

#### train yourself
It is fun to train yourself to use a dedicated email address and mobile number for your bot persona.
This enables you to create a complete identity with website, gitlab etc based around your new persona.


## Request a Developer account for your API Keys
Go to [Apply for access][twitter-dev-request] on the twitter developer page.

1. "What is your primary reason for using Twitter developer tools?"
    1. Professional ... for commercial uses
    1. Hobbyist ... for a personal project
    1. Academic ... for education or research
    1. Other, I don't fit any of those
1. I Choose: Hobbyist, making a bot.
1. "This is you, right?"
    1. When you logged in with your personal twitter account (see image above), 
twitter will pre-fill some fields, and present the opportunity to create a new twitter account.
I choose to create a new twitter account. 
I now have a personal identity and a developer identity at twitter.
    1. Note: a developer account needs a valid phone-number in addition to a valid email address, so when your twitter account does not yet have a valid phone-number now you will be requested for one.. [Twitter FAQ states][twitter-faq]: "You can add your phone number to up to 10 accounts."
1. "How will you use the Twitter API or Twitter data?"
    1. enter some details on what your main idea is. This is for twitter to be ok for mall-intent
1. "Is everything correct?"
    1. some summary page
1. "Please review and accept"
    1. End User Agreement legal document to accept
1. Now you will receive three things (1) API Key, (2) secret API key, (3) BEARER Token, and a screen how to test this.    

### Write down your KEYS.
Twitter is really pushing you to write down your three things.
I usually use [Google Keep](https://keep.google.com) and [LastPass Secure Notes](https://www.lastpass.com)
for writing down things that I want to be able to search and store secure in the cloud. 

My tip is to store it in LastPass Secure Notes, since they basically are passwords,
and having a password manager is always a good thing.

### What are the API key and the secret APT key?
An API Key is also known as a _public_ API key.  
A secret API Key is also known as a _private_ API key.
The secret/private key is your password. Nobody may know this.
Your public or normal key is what you can distribute freely.
Other computers will use your public key to _encrypt_ communication with your computer,
in such a way that your computer can only _decrypt_ or read it with your private key.

This is a way to make certain that only you can read the answers that are send to you.

If you want to know more on this, the internet is full of video's and tutorials 
on private/public key cryptography.

### What is the Bearer Token?
Basically the bearer token is just a unique identifier for twitter to identify your code (app/bot).

"This method is typically for developers that need read-only access to public information."" - [Twitter - OAuth 2.0 Bearer Token ](https://developer.twitter.com/en/docs/authentication/oauth-2-0)

### Twitter documentation.
Twitter has written great documentation on this. But these can be viewed as to-technical for some.
The main page shows when to use bearer Tokens and when other mains of identification/authorisation.

[Twitter - Authentication methods](https://developer.twitter.com/en/docs/authentication/overview) 
[Twitter - Application-only authentication and OAuth 2.0 Bearer Token](https://developer.twitter.com/en/docs/authentication/oauth-2-0/application-only
)
## Create twitter Account for your bot
I choose to create a new twitter account, but with my original email with the addition of a + tag.
[MATTHEW HUGHES][gmail] explains  in 2017 how to use the + symbol in your gmail email address, 
to create limitless virtual email addresses connected to your gmail address.
You can also create a separate Proton email address for your twitter account,
that is related to your developer account.


## Terminology clarification
from [Twitter Documentation](https://developer.twitter.com/en/docs/authentication/oauth-1-0a/obtaining-user-access-tokens)

### Client credentials

1. CONSUMER_KEY 
1. App Key
1. API Key
1. Consumer API Key
1. Consumer Key 
1. Customer Key
1. oauth_consumer_key

1. CONSUMER_SECRET 
1. App Key Secret
1. API Secret Key
1. Consumer Secret
1. Consumer Key
1. Customer Key
1. oauth_consumer_secret

1. Callback URL
1. oauth_callback
 
### Temporary credentials

1. Request Token
1. oauth_token

1. Request Token Secret
1. oauth_token_secret

1. oauth_verifier
 
### Token credentials

1. ACCESS_TOKEN
1. Access token
1. Token
1. resulting oauth_token

1. ACCESS_TOKEN_SECRET
1. Access token secret
1. Token Secret
1. resulting oauth_token_secret


[twitter-api-doc]: https://developer.twitter.com/en/docs/twitter-api/v1/tweets/search/overview
[twitter-api-doc-2]: https://developer.twitter.com/en/docs/twitter-api/getting-started/guide
[gmail]: https://thenextweb.com/google/2017/08/17/how-the-plus-sign-can-save-your-gmail-inbox-from-becoming-a-pit-of-doom/
[twitter-faq]: https://help.twitter.com/en/managing-your-account/phone-number-faqs


[twitter-dev-request]: https://developer.twitter.com/en/apply-for-access