import tweepy

# open a file called "keys" (no extention)
# with keys and tokens for Twitter
keyFile             = open('keys', 'r')
lines               = keyFile.readlines()
CONSUMER_KEY        = lines[0].rstrip()
CONSUMER_SECRET     = lines[1].rstrip()
ACCESS_TOKEN        = lines[2].rstrip()
ACCESS_TOKEN_SECRET = lines[3].rstrip()
keyFile.close()

# Authenticate to Twitter
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

# Create tweepy interface to twitter
api = tweepy.API(auth)

# post tweetText to twitter via the tweepy interface
tweetText = "Made with Tweepy"
tweet = api.update_status(tweetText)