import tweepy
import requests

# open a file called "keys" (no extention)
# with keys and tokens for Twitter
keyFile = open('keys', 'r')
lines = keyFile.readlines()
CONSUMER_KEY        = lines[0].rstrip()
CONSUMER_SECRET     = lines[1].rstrip()
ACCESS_TOKEN        = lines[2].rstrip()
ACCESS_TOKEN_SECRET = lines[3].rstrip()
keyFile.close()

url_joke = 'https://api.chucknorris.io/jokes/random'

# Authenticate to Twitter
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

# Create tweepy interface to twitter
api = tweepy.API(auth)

# Request a random joke
resp=requests.get(url_joke)
data = resp.json()

# post tweetText to twitter via the tweepy interface
tweetText = data["value"]
tweet = api.update_status(tweetText)